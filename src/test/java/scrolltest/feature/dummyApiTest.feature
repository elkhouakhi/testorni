@debug
Feature: test API dummy
Background:
* url 'https://dummy.restapiexample.com/api/v1'
Scenario: Get all employee data
Given path '/employees'
When method get
And print response
Then status 200
Scenario: Get a single employee data
Given path '/employee/1'
When method get
And print response
Then status 200
Scenario: Create new record in database
Given path '/create'
And request
"""
{
"name":"test",
"salary":"123",
"age":"23"
}
"""
When method post
And print response
Then status 200
Scenario: Update an employee record
Given path '/update/1'
And request
"""
{
"name":"test",
"salary":"123",
"age":"23"
}
"""
When method put
And print response
Then status 200
Scenario: delete an employee record
Given path '/delete/1'
When method delete
And print response
Then status 200